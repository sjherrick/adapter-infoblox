
## 1.7.0 [03-23-2020]

* add new call - also updated versions of dependencies

See merge request itentialopensource/adapters/inventory/adapter-infoblox!14

---

## 1.6.0 [03-05-2020]

* Minor/adapt 128

See merge request itentialopensource/adapters/inventory/adapter-infoblox!13

---

## 1.5.4 [02-25-2020]

* Patch/adapt 112

See merge request itentialopensource/adapters/inventory/adapter-infoblox!12

---

## 1.5.3 [01-28-2020]

* update the keys for some of the action results

See merge request itentialopensource/adapters/inventory/adapter-infoblox!10

---

## 1.5.2 [01-28-2020]

* update the pronghorn.json so that the parameter is properly sent as a number

See merge request itentialopensource/adapters/inventory/adapter-infoblox!9

---

## 1.5.1 [01-14-2020]

* december migration

See merge request itentialopensource/adapters/inventory/adapter-infoblox!8

---

## 1.5.0 [01-14-2020]

* Generic CRUD functions, healthcheck fix

See merge request itentialopensource/adapters/inventory/adapter-infoblox!7

---

## 1.4.0 [11-11-2019]

* update to the latest adapter foundation

See merge request itentialopensource/adapters/inventory/adapter-infoblox!6

---

## 1.3.0 [09-19-2019]

* september migration

See merge request itentialopensource/adapters/inventory/adapter-infoblox!4

---
## 1.2.0 [07-31-2019]
* Minor/migrateupdate

See merge request itentialopensource/adapters/inventory/adapter-infoblox!3

---

## 1.1.0 [07-31-2019]
* Minor/migrateupdate

See merge request itentialopensource/adapters/inventory/adapter-infoblox!3

---

## 1.0.6 [05-30-2019]
* Bug fixes and performance improvements

See commit ad204ae

---

# Current Version: 1.0.5 [04-02-2019]

## New Features

## Improvements
* __1.0.0 [11-16-2018]__ - Initial release of the Infoblox adapter! [PH-20154](http://itential.atlassian.net/browse/PH-20154)

## Bug Fixes
* __1.0.5 [04-02-2019]__ - Bug fixes and performance improvements [PH-30683](http://itential.atlassian.net/browse/PH-30683)
* __1.0.4 [02-28-2019]__ - Remove test directory from output [NS-379](http://itential.atlassian.net/browse/NS-379)
* __1.0.1 [11-27-2018]__ - Fixed encoding of the path variables. [PH-21440](http://itential.atlassian.net/browse/PH-21440)

## Deprecation

## Security


---

* __1.0.0 [11-16-2018]__ - Initial release of the Infoblox adapter! [PH-20154](http://itential.atlassian.net/browse/PH-20154)
