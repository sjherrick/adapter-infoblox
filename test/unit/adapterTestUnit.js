/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint global-require: warn */
/* eslint no-unused-vars: warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs-extra');
const mocha = require('mocha');
const path = require('path');
const util = require('util');
const winston = require('winston');
const execute = require('child_process').execSync;
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const stub = true;
const isRapidFail = false;
const attemptTimeout = 120000;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
const host = 'replace.hostorip.here';
const username = 'username';
const password = 'password';
const protocol = 'http';
const port = 80;
const sslenable = false;
const sslinvalid = false;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'infoblox',
      type: 'Infoblox',
      properties: {
        host,
        port,
        base_path: '/wapi',
        version: 'v1',
        cache_location: 'none',
        save_metric: false,
        stub,
        protocol,
        authentication: {
          auth_method: 'basic user_password',
          username,
          password,
          token: '',
          invalid_token_error: 401,
          token_timeout: -1,
          token_cache: 'local',
          auth_field: 'header.headers.Authorization',
          auth_field_format: 'Basic {b64}{username}:{password}{/b64}'
        },
        healthcheck: {
          type: 'startup',
          frequency: 60000
        },
        throttle: {
          throttle_enabled: false,
          number_pronghorns: 1,
          sync_async: 'sync',
          max_in_queue: 1000,
          concurrent_max: 1,
          expire_timeout: 0,
          avg_runtime: 200
        },
        request: {
          number_redirects: 0,
          number_retries: 3,
          limit_retry_error: 0,
          failover_codes: [],
          attempt_timeout: attemptTimeout,
          global_request: {
            payload: {},
            uriOptions: {},
            addlHeaders: {},
            authData: {}
          },
          healthcheck_on_timeout: false,
          return_raw: true,
          archiving: false
        },
        proxy: {
          enabled: false,
          host: '',
          port: 1,
          protocol: 'http'
        },
        ssl: {
          ecdhCurve: '',
          enabled: sslenable,
          accept_invalid_cert: sslinvalid,
          ca_file: '',
          key_file: '',
          cert_file: '',
          secure_protocol: '',
          ciphers: ''
        },
        mongo: {
          host: '',
          port: 0,
          database: '',
          username: '',
          password: '',
          replSet: '',
          db_ssl: {
            enabled: false,
            accept_invalid_cert: false,
            ca_file: '',
            key_file: '',
            cert_file: ''
          }
        }
      }
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}


// require the adapter that we are going to be using
const Infoblox = require('../../adapter.js');

// delete the .DS_Store directory in entities -- otherwise this will cause errors
const dirPath = path.join(__dirname, '../../entities/.DS_Store');
if (fs.existsSync(dirPath)) {
  try {
    fs.removeSync(dirPath);
    console.log('.DS_Store deleted');
  } catch (e) {
    console.log('Error when deleting .DS_Store:', e);
  }
}

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[unit] Infoblox Adapter Test', () => {
  describe('Infoblox Class Tests', () => {
    const a = new Infoblox(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('adapterBase.js', () => {
      it('should have an adapterBase.js', (done) => {
        try {
          fs.exists('adapterBase.js', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    let wffunctions = [];
    describe('#getWorkflowFunctions', () => {
      it('should retrieve workflow functions', (done) => {
        try {
          wffunctions = a.getWorkflowFunctions();

          try {
            assert.notEqual(0, wffunctions.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('package.json', () => {
      it('should have a package.json', (done) => {
        try {
          fs.exists('package.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be validated', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          const { PJV } = require('package-json-validator');
          const options = {
            warnings: true, // show warnings
            recommendations: true // show recommendations
          };
          const results = PJV.validate(JSON.stringify(packageDotJson), 'npm', options);

          if (results.valid === false) {
            log.error('The package.json contains the following errors: ');
            log.error(util.inspect(results));
            assert.equal(true, results.valid);
          } else {
            assert.equal(true, results.valid);
          }

          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('package.json should be customized', (done) => {
        try {
          const packageDotJson = require('../../package.json');
          assert.notEqual(-1, packageDotJson.name.indexOf('infoblox'));
          assert.notEqual(undefined, packageDotJson.version);
          assert.notEqual(null, packageDotJson.version);
          assert.notEqual('', packageDotJson.version);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('pronghorn.json', () => {
      it('should have a pronghorn.json', (done) => {
        try {
          fs.exists('pronghorn.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should be customized', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          assert.notEqual(-1, pronghornDotJson.id.indexOf('infoblox'));
          assert.equal('Infoblox', pronghornDotJson.export);
          assert.equal('Infoblox', pronghornDotJson.title);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('pronghorn.json should only expose workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');

          for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
            let found = false;
            let paramissue = false;

            for (let w = 0; w < wffunctions.length; w += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                const methLine = execute(`grep "  ${wffunctions[w]}(" adapter.js | grep "callback) {"`).toString();
                let wfparams = [];

                if (methLine && methLine.indexOf('(') >= 0 && methLine.indexOf(')') >= 0) {
                  const temp = methLine.substring(methLine.indexOf('(') + 1, methLine.indexOf(')'));
                  wfparams = temp.split(',');

                  for (let t = 0; t < wfparams.length; t += 1) {
                    // remove default value from the parameter name
                    wfparams[t] = wfparams[t].substring(0, wfparams[t].search(/=/) > 0 ? wfparams[t].search(/#|\?|=/) : wfparams[t].length);
                    // remove spaces
                    wfparams[t] = wfparams[t].trim();

                    if (wfparams[t] === 'callback') {
                      wfparams.splice(t, 1);
                    }
                  }
                }

                // if there are inputs defined but not on the method line
                if (wfparams.length === 0 && (pronghornDotJson.methods[m].input
                    && pronghornDotJson.methods[m].input.length > 0)) {
                  paramissue = true;
                } else if (wfparams.length > 0 && (!pronghornDotJson.methods[m].input
                    || pronghornDotJson.methods[m].input.length === 0)) {
                  // if there are no inputs defined but there are on the method line
                  paramissue = true;
                } else {
                  for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                    let pfound = false;
                    for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                  for (let wfp = 0; wfp < wfparams.length; wfp += 1) {
                    let pfound = false;
                    for (let p = 0; p < pronghornDotJson.methods[m].input.length; p += 1) {
                      if (pronghornDotJson.methods[m].input[p].name.toUpperCase() === wfparams[wfp].toUpperCase()) {
                        pfound = true;
                      }
                    }

                    if (!pfound) {
                      paramissue = true;
                    }
                  }
                }

                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} not found in workflow functions`);
            }
            if (paramissue) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${pronghornDotJson.methods[m].name} has a parameter mismatch`);
            }
            assert.equal(true, found);
            assert.equal(false, paramissue);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('pronghorn.json should expose all workflow functions', (done) => {
        try {
          const pronghornDotJson = require('../../pronghorn.json');
          for (let w = 0; w < wffunctions.length; w += 1) {
            let found = false;

            for (let m = 0; m < pronghornDotJson.methods.length; m += 1) {
              if (pronghornDotJson.methods[m].name === wffunctions[w]) {
                found = true;
                break;
              }
            }

            if (!found) {
              // this is the reason to go through both loops - log which ones are not found so
              // they can be worked
              log.error(`${wffunctions[w]} not found in pronghorn.json`);
            }
            assert.equal(true, found);
          }
          done();
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('propertiesSchema.json', () => {
      it('should have a propertiesSchema.json', (done) => {
        try {
          fs.exists('propertiesSchema.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('propertiesSchema.json should be customized', (done) => {
        try {
          const propertiesDotJson = require('../../propertiesSchema.json');
          assert.equal('adapter-infoblox', propertiesDotJson.$id);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('error.json', () => {
      it('should have an error.json', (done) => {
        try {
          fs.exists('error.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('sampleProperties.json', () => {
      it('should have a sampleProperties.json', (done) => {
        try {
          fs.exists('sampleProperties.json', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkProperties', () => {
      it('should have a checkProperties function', (done) => {
        try {
          assert.equal(true, typeof a.checkProperties === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the sample properties should be good - if failure change the log level', (done) => {
        try {
          const samplePropsJson = require('../../sampleProperties.json');
          const clean = a.checkProperties(samplePropsJson.properties);

          try {
            assert.notEqual(0, Object.keys(clean));
            assert.equal(undefined, clean.exception);
            assert.notEqual(undefined, clean.host);
            assert.notEqual(null, clean.host);
            assert.notEqual('', clean.host);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('README.md', () => {
      it('should have a README', (done) => {
        try {
          fs.exists('README.md', (val) => {
            assert.equal(true, val);
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('README.md should be customized', (done) => {
        try {
          fs.readFile('README.md', 'utf8', (err, data) => {
            assert.equal(-1, data.indexOf('[System]'));
            assert.equal(-1, data.indexOf('[system]'));
            assert.equal(-1, data.indexOf('[version]'));
            assert.equal(-1, data.indexOf('[namespace]'));
            done();
          });
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#connect', () => {
      it('should have a connect function', (done) => {
        try {
          assert.equal(true, typeof a.connect === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should have a healthCheck function', (done) => {
        try {
          assert.equal(true, typeof a.healthCheck === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
    });

    describe('#checkActionFiles', () => {
      it('should have a checkActionFiles function', (done) => {
        try {
          assert.equal(true, typeof a.checkActionFiles === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('the action files should be good - if failure change the log level as most issues are warnings', (done) => {
        try {
          const clean = a.checkActionFiles();

          try {
            for (let c = 0; c < clean.length; c += 1) {
              log.error(clean[c]);
            }
            assert.equal(0, clean.length);
            done();
          } catch (err) {
            log.error(`Test Failure: ${err}`);
            done(err);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#encryptProperty', () => {
      it('should have a encryptProperty function', (done) => {
        try {
          assert.equal(true, typeof a.encryptProperty === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      });
      it('should get base64 encoded property', (done) => {
        try {
          a.encryptProperty('testing', 'base64', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{code}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should get encrypted property', (done) => {
        try {
          a.encryptProperty('testing', 'encrypt', (data, error) => {
            try {
              assert.equal(undefined, error);
              assert.notEqual(undefined, data);
              assert.notEqual(null, data);
              assert.notEqual(undefined, data.response);
              assert.notEqual(null, data.response);
              assert.equal(0, data.response.indexOf('{crypt}'));
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // describe('#hasEntity', () => {
    //   it('should have a hasEntity function', (done) => {
    //     try {
    //       assert.equal(true, typeof a.hasEntity === 'function');
    //       done();
    //     } catch (error) {
    //       log.error(`Test Failure: ${error}`);
    //       done(error);
    //     }
    //   });
    //   it('should find entity', (done) => {
    //     try {
    //       a.hasEntity('template_entity', // 'a9e9c33dc61122760072455df62663d2', (data) => {
    //         try {
    //           assert.equal(true, data[0]);
    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    //   it('should not find entity', (done) => {
    //     try {
    //       a.hasEntity('template_entity', 'blah', (data) => {
    //         try {
    //           assert.equal(false, data[0]);
    //           done();
    //         } catch (err) {
    //           log.error(`Test Failure: ${err}`);
    //           done(err);
    //         }
    //       });
    //     } catch (error) {
    //       log.error(`Adapter Exception: ${error}`);
    //       done(error);
    //     }
    //   }).timeout(attemptTimeout);
    // });

    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */

    describe('#createNetwork - errors', () => {
      it('should have a createNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.createNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no network', (done) => {
        try {
          a.createNetwork(null, null, (data, error) => {
            try {
              const displayE = 'network is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no comment', (done) => {
        try {
          a.createNetwork('1.1.1.1', null, (data, error) => {
            try {
              const displayE = 'comment is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkKeyByIP - errors', () => {
      it('should have a getNetworkKeyByIP function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkKeyByIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no networkIP', (done) => {
        try {
          a.getNetworkKeyByIP(null, (data, error) => {
            try {
              const displayE = 'networkIP is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getNetworkKeyByIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAuthZone - errors', () => {
      it('should have a createAuthZone function', (done) => {
        try {
          assert.equal(true, typeof a.createAuthZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no fqdnName', (done) => {
        try {
          a.createAuthZone(null, (data, error) => {
            try {
              const displayE = 'fqdnName is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createAuthZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHostRecord - errors', () => {
      it('should have a createHostRecord function', (done) => {
        try {
          assert.equal(true, typeof a.createHostRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no hostName', (done) => {
        try {
          a.createHostRecord(null, null, null, (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createHostRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no IPAddress', (done) => {
        try {
          a.createHostRecord('itential.com', null, null, (data, error) => {
            try {
              const displayE = 'IPAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createHostRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createHostRecord2 - errors', () => {
      it('should have a createHostRecord2 function', (done) => {
        try {
          assert.equal(true, typeof a.createHostRecord2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no hostName', (done) => {
        try {
          a.createHostRecord2(null, null, null, (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createHostRecord2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no IPAddress', (done) => {
        try {
          a.createHostRecord2('itential.com', null, null, (data, error) => {
            try {
              const displayE = 'IPAddress is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createHostRecord2', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpByHost - errors', () => {
      it('should have a getIpByHost function', (done) => {
        try {
          assert.equal(true, typeof a.getIpByHost === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no hostName', (done) => {
        try {
          a.getIpByHost(null, (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getIpByHost', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostKeyByHostName - errors', () => {
      it('should have a getHostKeyByHostName function', (done) => {
        try {
          assert.equal(true, typeof a.getHostKeyByHostName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no hostName', (done) => {
        try {
          a.getHostKeyByHostName(null, (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getHostKeyByHostName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHostRecordByHostName - errors', () => {
      it('should have a deleteHostRecordByHostName function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHostRecordByHostName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no hostName', (done) => {
        try {
          a.deleteHostRecordByHostName(null, (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-deleteHostRecordByHostName', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#assignNextIP - errors', () => {
      it('should have a assignNextIP function', (done) => {
        try {
          assert.equal(true, typeof a.assignNextIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no networkIP', (done) => {
        try {
          a.assignNextIP(null, ' ', ' ', (data, error) => {
            try {
              const displayE = 'networkIP is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-assignNextIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no hostName', (done) => {
        try {
          a.assignNextIP(' ', null, ' ', (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-assignNextIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - no fqdnName', (done) => {
        try {
          a.assignNextIP(' ', ' ', null, (data, error) => {
            try {
              const displayE = 'fqdnName is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-assignNextIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAuthZoneDetailsByfqdnName - errors', () => {
      it('should have a getAuthZoneDetailsByfqdnName function', (done) => {
        try {
          assert.equal(true, typeof a.getAuthZoneDetailsByfqdnName === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing fqdn', (done) => {
        try {
          a.getAuthZoneDetailsByfqdnName(null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on should be string';
              runErrorAsserts(data, error, 'AD.312', 'infoblox-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getForwardZones - errors', () => {
      it('should have a getForwardZones function', (done) => {
        try {
          assert.equal(true, typeof a.getForwardZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createForwardZone - errors', () => {
      it('should have a createForwardZone function', (done) => {
        try {
          assert.equal(true, typeof a.createForwardZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createForwardZone(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createForwardZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteZones - errors', () => {
      it('should have a deleteZones function', (done) => {
        try {
          assert.equal(true, typeof a.deleteZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.deleteZones(null, (data, error) => {
            try {
              const displayE = 'objectReference is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-deleteZones', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getZoneDelegations - errors', () => {
      it('should have a getZoneDelegations function', (done) => {
        try {
          assert.equal(true, typeof a.getZoneDelegations === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createZoneDelegation - errors', () => {
      it('should have a createZoneDelegation function', (done) => {
        try {
          assert.equal(true, typeof a.createZoneDelegation === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createZoneDelegation(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createZoneDelegation', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteAuthZoneByRef - errors', () => {
      it('should have a deleteAuthZoneByRef function', (done) => {
        try {
          assert.equal(true, typeof a.deleteAuthZoneByRef === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zoneRef', (done) => {
        try {
          a.deleteAuthZoneByRef(null, (data, error) => {
            try {
              const displayE = 'zoneRef is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-deleteAuthZoneByRef', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkBlock - errors', () => {
      it('should have a getNetworkBlock function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.getNetworkBlock(null, (data, error) => {
            try {
              const displayE = 'objectReference is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getNetworkBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#modifyNetworkBlock - errors', () => {
      it('should have a modifyNetworkBlock function', (done) => {
        try {
          assert.equal(true, typeof a.modifyNetworkBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.modifyNetworkBlock(null, null, (data, error) => {
            try {
              const displayE = 'networkIP is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getNetworkKeyByIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.modifyNetworkBlock('fakeparam', null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on should be string';
              runErrorAsserts(data, error, 'AD.312', 'infoblox-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkDetails - errors', () => {
      it('should have a getNetworkDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing network', (done) => {
        try {
          a.getNetworkDetails(null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on should be string';
              runErrorAsserts(data, error, 'AD.312', 'infoblox-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkContainerDetails - errors', () => {
      it('should have a getNetworkContainerDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkContainerDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing network', (done) => {
        try {
          a.getNetworkContainerDetails(null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on should be string';
              runErrorAsserts(data, error, 'AD.312', 'infoblox-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkContainer - errors', () => {
      it('should have a createNetworkContainer function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkContainer - errors', () => {
      it('should have a deleteNetworkContainer function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkContainer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkRef', (done) => {
        try {
          a.deleteNetworkContainer(null, (data, error) => {
            try {
              const displayE = 'networkRef is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-deleteNetworkContainer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#genericCreateNextAvailableNetwork - errors', () => {
      it('should have a genericCreateNextAvailableNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.genericCreateNextAvailableNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkRef', (done) => {
        try {
          a.genericCreateNextAvailableNetwork(null, null, null, (data, error) => {
            try {
              const displayE = 'networkRef is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-genericCreateNextAvailableNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing options', (done) => {
        try {
          a.genericCreateNextAvailableNetwork('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'options is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-genericCreateNextAvailableNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.genericCreateNextAvailableNetwork('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-genericCreateNextAvailableNetwork', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetwork - errors', () => {
      it('should have a deleteNetwork function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetwork === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkRef', (done) => {
        try {
          a.deleteNetwork(null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on should be string';
              runErrorAsserts(data, error, 'AD.312', 'infoblox-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteNetworkv2 - errors', () => {
      it('should have a deleteNetworkv2 function', (done) => {
        try {
          assert.equal(true, typeof a.deleteNetworkv2 === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing networkRef', (done) => {
        try {
          a.deleteNetworkv2(null, (data, error) => {
            try {
              const displayE = 'Schema validation failed on should be string';
              runErrorAsserts(data, error, 'AD.312', 'infoblox-translatorUtil-buildJSONEntity', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkUtilizationByIP - errors', () => {
      it('should have a getNetworkUtilizationByIP function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkUtilizationByIP === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing network', (done) => {
        try {
          a.getNetworkUtilizationByIP(null, (data, error) => {
            try {
              const displayE = 'networkIP is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getNetworkUtilizationByIP', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkBlock - errors', () => {
      it('should have a createNetworkBlock function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkBlock === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing function', (done) => {
        try {
          a.createNetworkBlock(null, null, null, (data, error) => {
            try {
              const displayE = 'functionParam is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createNetworkBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.createNetworkBlock('fakeparam', null, null, (data, error) => {
            try {
              const displayE = 'objectReference is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createNetworkBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNetworkBlock('fakeparam', 'fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createNetworkBlock', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDtcServer - errors', () => {
      it('should have a getDtcServer function', (done) => {
        try {
          assert.equal(true, typeof a.getDtcServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDtcServer - errors', () => {
      it('should have a createDtcServer function', (done) => {
        try {
          assert.equal(true, typeof a.createDtcServer === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDtcServer(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createDtcServer', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDtcPool - errors', () => {
      it('should have a getDtcPool function', (done) => {
        try {
          assert.equal(true, typeof a.getDtcPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDtcPool - errors', () => {
      it('should have a createDtcPool function', (done) => {
        try {
          assert.equal(true, typeof a.createDtcPool === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDtcPool(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createDtcPool', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createDtcLbdn - errors', () => {
      it('should have a createDtcLbdn function', (done) => {
        try {
          assert.equal(true, typeof a.createDtcLbdn === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createDtcLbdn(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createDtcLbdn', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getHostRecord - errors', () => {
      it('should have a getHostRecord function', (done) => {
        try {
          assert.equal(true, typeof a.getHostRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing hostName', (done) => {
        try {
          a.getHostRecord(null, (data, error) => {
            try {
              const displayE = 'hostName is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getHostRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getARecords - errors', () => {
      it('should have a getARecords function', (done) => {
        try {
          assert.equal(true, typeof a.getARecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createARecord - errors', () => {
      it('should have a createARecord function', (done) => {
        try {
          assert.equal(true, typeof a.createARecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createARecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createARecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getCnameRecords - errors', () => {
      it('should have a getCnameRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getCnameRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createCNAMERecord - errors', () => {
      it('should have a createCNAMERecord function', (done) => {
        try {
          assert.equal(true, typeof a.createCNAMERecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createCNAMERecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createCNAMERecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getTxtRecords - errors', () => {
      it('should have a getTxtRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getTxtRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createTxtRecord - errors', () => {
      it('should have a createTxtRecord function', (done) => {
        try {
          assert.equal(true, typeof a.createTxtRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createTxtRecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createTxtRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRecords - errors', () => {
      it('should have a getAllRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zone', (done) => {
        try {
          a.getAllRecords(null, (data, error) => {
            try {
              const displayE = 'zone is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getAllRecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAaaaRecords - errors', () => {
      it('should have a getAaaaRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getAaaaRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createAaaaRecord - errors', () => {
      it('should have a createAaaaRecord function', (done) => {
        try {
          assert.equal(true, typeof a.createAaaaRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createAaaaRecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createAaaaRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMxRecords - errors', () => {
      it('should have a getMxRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getMxRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMxRecord - errors', () => {
      it('should have a createMxRecord function', (done) => {
        try {
          assert.equal(true, typeof a.createMxRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createMxRecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createMxRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNsRecords - errors', () => {
      it('should have a getNsRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getNsRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNsRecord - errors', () => {
      it('should have a createNsRecord function', (done) => {
        try {
          assert.equal(true, typeof a.createNsRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNsRecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createNsRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPtrRecords - errors', () => {
      it('should have a getPtrRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getPtrRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createPtrRecord - errors', () => {
      it('should have a createPtrRecord function', (done) => {
        try {
          assert.equal(true, typeof a.createPtrRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createPtrRecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createPtrRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getSrvRecords - errors', () => {
      it('should have a getSrvRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getSrvRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSrvRecord - errors', () => {
      it('should have a createSrvRecord function', (done) => {
        try {
          assert.equal(true, typeof a.createSrvRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSrvRecord(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createSrvRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateHostRecord - errors', () => {
      it('should have a updateHostRecord function', (done) => {
        try {
          assert.equal(true, typeof a.updateHostRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.updateHostRecord(null, null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-updateHostRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateHostRecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-updateHostRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteHostRecord - errors', () => {
      it('should have a deleteHostRecord function', (done) => {
        try {
          assert.equal(true, typeof a.deleteHostRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.deleteHostRecord(null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-deleteHostRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateARecord - errors', () => {
      it('should have a updateARecord function', (done) => {
        try {
          assert.equal(true, typeof a.updateARecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.updateARecord(null, null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-updateARecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateARecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-updateARecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteARecord - errors', () => {
      it('should have a deleteARecord function', (done) => {
        try {
          assert.equal(true, typeof a.deleteARecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.deleteARecord(null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-deleteARecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updatePTRRecord - errors', () => {
      it('should have a updatePTRRecord function', (done) => {
        try {
          assert.equal(true, typeof a.updatePTRRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.updatePTRRecord(null, null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-updatePTRRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updatePTRRecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-updatePTRRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deletePTRRecord - errors', () => {
      it('should have a deletePTRRecord function', (done) => {
        try {
          assert.equal(true, typeof a.deletePTRRecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.deletePTRRecord(null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-deletePTRRecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateCNAMERecord - errors', () => {
      it('should have a updateCNAMERecord function', (done) => {
        try {
          assert.equal(true, typeof a.updateCNAMERecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.updateCNAMERecord(null, null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-updateCNAMERecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateCNAMERecord('fakeparam', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-updateCNAMERecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteCNAMERecord - errors', () => {
      it('should have a deleteCNAMERecord function', (done) => {
        try {
          assert.equal(true, typeof a.deleteCNAMERecord === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing recordkey', (done) => {
        try {
          a.deleteCNAMERecord(null, (data, error) => {
            try {
              const displayE = 'recordkey is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-deleteCNAMERecord', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGridDns - errors', () => {
      it('should have a getGridDns function', (done) => {
        try {
          assert.equal(true, typeof a.getGridDns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMemberDns - errors', () => {
      it('should have a getMemberDns function', (done) => {
        try {
          assert.equal(true, typeof a.getMemberDns === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getResponsePolicyZones - errors', () => {
      it('should have a getResponsePolicyZones function', (done) => {
        try {
          assert.equal(true, typeof a.getResponsePolicyZones === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createResponsePolicyZone - errors', () => {
      it('should have a createResponsePolicyZone function', (done) => {
        try {
          assert.equal(true, typeof a.createResponsePolicyZone === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createResponsePolicyZone(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createResponsePolicyZone', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createSubstituitionRuleForARecords - errors', () => {
      it('should have a createSubstituitionRuleForARecords function', (done) => {
        try {
          assert.equal(true, typeof a.createSubstituitionRuleForARecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createSubstituitionRuleForARecords(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createSubstituitionRuleForARecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSubstituitionRuleForPtrRecords - errors', () => {
      it('should have a addSubstituitionRuleForPtrRecords function', (done) => {
        try {
          assert.equal(true, typeof a.addSubstituitionRuleForPtrRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSubstituitionRuleForPtrRecords(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-addSubstituitionRuleForPtrRecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSubstituitionRuleForIpTriggerPolicy - errors', () => {
      it('should have a addSubstituitionRuleForIpTriggerPolicy function', (done) => {
        try {
          assert.equal(true, typeof a.addSubstituitionRuleForIpTriggerPolicy === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSubstituitionRuleForIpTriggerPolicy(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-addSubstituitionRuleForIpTriggerPolicy', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBlockDomainNameRule - errors', () => {
      it('should have a addBlockDomainNameRule function', (done) => {
        try {
          assert.equal(true, typeof a.addBlockDomainNameRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addBlockDomainNameRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-addBlockDomainNameRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBlockClientIpAddressRule - errors', () => {
      it('should have a addBlockClientIpAddressRule function', (done) => {
        try {
          assert.equal(true, typeof a.addBlockClientIpAddressRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addBlockClientIpAddressRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-addBlockClientIpAddressRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSubstituteDomainNameClientIpAddressRule - errors', () => {
      it('should have a addSubstituteDomainNameClientIpAddressRule function', (done) => {
        try {
          assert.equal(true, typeof a.addSubstituteDomainNameClientIpAddressRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSubstituteDomainNameClientIpAddressRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-addSubstituteDomainNameClientIpAddressRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addBlockIpAddressNoSuchDomainRule - errors', () => {
      it('should have a addBlockIpAddressNoSuchDomainRule function', (done) => {
        try {
          assert.equal(true, typeof a.addBlockIpAddressNoSuchDomainRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addBlockIpAddressNoSuchDomainRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-addBlockIpAddressNoSuchDomainRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#addSubstituteDomainNameIpAddressRule - errors', () => {
      it('should have a addSubstituteDomainNameIpAddressRule function', (done) => {
        try {
          assert.equal(true, typeof a.addSubstituteDomainNameIpAddressRule === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.addSubstituteDomainNameIpAddressRule(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-addSubstituteDomainNameIpAddressRule', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getAllRpzRecords - errors', () => {
      it('should have a getAllRpzRecords function', (done) => {
        try {
          assert.equal(true, typeof a.getAllRpzRecords === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing zone', (done) => {
        try {
          a.getAllRpzRecords(null, (data, error) => {
            try {
              const displayE = 'zone is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getAllRpzRecords', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNameServerGroups - errors', () => {
      it('should have a getNameServerGroups function', (done) => {
        try {
          assert.equal(true, typeof a.getNameServerGroups === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNameServerGroup - errors', () => {
      it('should have a createNameServerGroup function', (done) => {
        try {
          assert.equal(true, typeof a.createNameServerGroup === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNameServerGroup(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createNameServerGroup', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getRangeByExtensibleAttribute - errors', () => {
      it('should have a getRangeByExtensibleAttribute function', (done) => {
        try {
          assert.equal(true, typeof a.getRangeByExtensibleAttribute === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing location', (done) => {
        try {
          a.getRangeByExtensibleAttribute(null, (data, error) => {
            try {
              const displayE = 'location is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getRangeByExtensibleAttribute', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createRange - errors', () => {
      it('should have a createRange function', (done) => {
        try {
          assert.equal(true, typeof a.createRange === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createRange(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createRange', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getLeaseByIpAddress - errors', () => {
      it('should have a getLeaseByIpAddress function', (done) => {
        try {
          assert.equal(true, typeof a.getLeaseByIpAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing address', (done) => {
        try {
          a.getLeaseByIpAddress(null, (data, error) => {
            try {
              const displayE = 'address is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getLeaseByIpAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMemberDhcp - errors', () => {
      it('should have a getMemberDhcp function', (done) => {
        try {
          assert.equal(true, typeof a.getMemberDhcp === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpDetails - errors', () => {
      it('should have a getIpDetails function', (done) => {
        try {
          assert.equal(true, typeof a.getIpDetails === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getIpAddressUsingSearch - errors', () => {
      it('should have a getIpAddressUsingSearch function', (done) => {
        try {
          assert.equal(true, typeof a.getIpAddressUsingSearch === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing address', (done) => {
        try {
          a.getIpAddressUsingSearch(null, (data, error) => {
            try {
              const displayE = 'address is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getIpAddressUsingSearch', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getDnsView - errors', () => {
      it('should have a getDnsView function', (done) => {
        try {
          assert.equal(true, typeof a.getDnsView === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getNetworkView - errors', () => {
      it('should have a getNetworkView function', (done) => {
        try {
          assert.equal(true, typeof a.getNetworkView === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createNetworkView - errors', () => {
      it('should have a createNetworkView function', (done) => {
        try {
          assert.equal(true, typeof a.createNetworkView === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createNetworkView(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createNetworkView', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getFixedAddressMac - errors', () => {
      it('should have a getFixedAddressMac function', (done) => {
        try {
          assert.equal(true, typeof a.getFixedAddressMac === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing mac', (done) => {
        try {
          a.getFixedAddressMac(null, (data, error) => {
            try {
              const displayE = 'mac is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getFixedAddressMac', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createFixedAddress - errors', () => {
      it('should have a createFixedAddress function', (done) => {
        try {
          assert.equal(true, typeof a.createFixedAddress === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createFixedAddress(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createFixedAddress', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getMembers - errors', () => {
      it('should have a getMembers function', (done) => {
        try {
          assert.equal(true, typeof a.getMembers === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createMember - errors', () => {
      it('should have a createMember function', (done) => {
        try {
          assert.equal(true, typeof a.createMember === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createMember(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createMember', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGrid - errors', () => {
      it('should have a getGrid function', (done) => {
        try {
          assert.equal(true, typeof a.getGrid === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getGridPendingChanges - errors', () => {
      it('should have a getGridPendingChanges function', (done) => {
        try {
          assert.equal(true, typeof a.getGridPendingChanges === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getVdiscoveryTasks - errors', () => {
      it('should have a getVdiscoveryTasks function', (done) => {
        try {
          assert.equal(true, typeof a.getVdiscoveryTasks === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createVdiscoveryTask - errors', () => {
      it('should have a createVdiscoveryTask function', (done) => {
        try {
          assert.equal(true, typeof a.createVdiscoveryTask === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createVdiscoveryTask(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createVdiscoveryTask', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getPermissionsForARole - errors', () => {
      it('should have a getPermissionsForARole function', (done) => {
        try {
          assert.equal(true, typeof a.getPermissionsForARole === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing role', (done) => {
        try {
          a.getPermissionsForARole(null, (data, error) => {
            try {
              const displayE = 'role is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getPermissionsForARole', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getWapiSchema - errors', () => {
      it('should have a getWapiSchema function', (done) => {
        try {
          assert.equal(true, typeof a.getWapiSchema === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing schema', (done) => {
        try {
          a.getWapiSchema(null, (data, error) => {
            try {
              const displayE = 'schema is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getWapiSchema', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#multipleRecordTypes - errors', () => {
      it('should have a multipleRecordTypes function', (done) => {
        try {
          assert.equal(true, typeof a.multipleRecordTypes === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.multipleRecordTypes(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-multipleRecordTypes', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getExtensibleAttributeDefinition - errors', () => {
      it('should have a getExtensibleAttributeDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.getExtensibleAttributeDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createExtensibleAttributeDefinition - errors', () => {
      it('should have a createExtensibleAttributeDefinition function', (done) => {
        try {
          assert.equal(true, typeof a.createExtensibleAttributeDefinition === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createExtensibleAttributeDefinition(null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createExtensibleAttributeDefinition', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#createObject - errors', () => {
      it('should have a createObject function', (done) => {
        try {
          assert.equal(true, typeof a.createObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectType', (done) => {
        try {
          a.createObject(null, {}, (data, error) => {
            try {
              const displayE = 'objectType is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.createObject('type', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-createObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#updateObject - errors', () => {
      it('should have a updateObject function', (done) => {
        try {
          assert.equal(true, typeof a.updateObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.updateObject(null, {}, (data, error) => {
            try {
              const displayE = 'objectReference is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-updateObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      // we're expecting a slash in the objectReference to extract the object type. e.g. record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS5pbmZvLmhvc3Qx
      it('should error if - missing slash from objectReference', (done) => {
        try {
          a.updateObject('noslash', {}, (data, error) => {
            try {
              const errorMessage = 'extractObjectTypeFromObjectReference: could not extract type, expecting slash in objectReference';
              assert.equal(error, errorMessage);
              assert.equal(data, null);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing body', (done) => {
        try {
          a.updateObject('type', null, (data, error) => {
            try {
              const displayE = 'body is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-updateObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#deleteObject - errors', () => {
      it('should have a deleteObject function', (done) => {
        try {
          assert.equal(true, typeof a.deleteObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.deleteObject(null, (data, error) => {
            try {
              const displayE = 'objectReference is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-deleteObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      // we're expecting a slash in the objectReference to extract the object type. e.g. record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS5pbmZvLmhvc3Qx
      it('should error if - missing slash from objectReference', (done) => {
        try {
          a.deleteObject('noslash', (data, error) => {
            try {
              const errorMessage = 'extractObjectTypeFromObjectReference: could not extract type, expecting slash in objectReference';
              assert.equal(error, errorMessage);
              assert.equal(data, null);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#getObject - errors', () => {
      it('should have a getObject function', (done) => {
        try {
          assert.equal(true, typeof a.getObject === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectType', (done) => {
        try {
          a.getObject(null, {}, 'field', (data, error) => {
            try {
              const displayE = 'objectType is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing queryObject', (done) => {
        try {
          a.getObject('type', null, 'field', (data, error) => {
            try {
              const displayE = 'queryObject is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing returnFields', (done) => {
        try {
          a.getObject('type', {}, null, (data, error) => {
            try {
              const displayE = 'returnFields is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-getObject', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#extractObjectTypeFromObjectReference - errors', () => {
      it('should have a extractObjectTypeFromObjectReference function', (done) => {
        try {
          assert.equal(true, typeof a.extractObjectTypeFromObjectReference === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.extractObjectTypeFromObjectReference(null, (data, error) => {
            try {
              const displayE = 'objectReference is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-extractObjectTypeFromObjectReference', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      // we're expecting a slash in the objectReference to extract the object type. e.g. record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS5pbmZvLmhvc3Qx
      it('should error if - missing slash from objectReference', (done) => {
        try {
          a.extractObjectTypeFromObjectReference('noslash', (data, error) => {
            try {
              const errorMessage = 'extractObjectTypeFromObjectReference: could not extract type, expecting slash in objectReference';
              assert.equal(error, errorMessage);
              assert.equal(data, null);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      const objectReference = 'record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS5pbmZvLmhvc3Qx';
      it('should return correct object type', (done) => {
        try {
          a.extractObjectTypeFromObjectReference(objectReference, (data, error) => {
            try {
              assert.equal(data, 'record:host');
              assert.equal(error, null);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#restartServices - errors', () => {
      it('should have a restartServices function', (done) => {
        try {
          assert.equal(true, typeof a.restartServices === 'function');
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      it('should error if - missing objectReference', (done) => {
        try {
          a.restartServices(null, null, (data, error) => {
            try {
              const displayE = 'objectReference is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-restartServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      const objectReference = 'grid/b25lLmNsdXN0ZXIkMA:Infoblox';
      it('should error if - missing queries', (done) => {
        try {
          a.restartServices(objectReference, null, (data, error) => {
            try {
              const displayE = 'queries is required';
              runErrorAsserts(data, error, 'AD.300', 'infoblox-adapter-restartServices', displayE);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
      // we're expecting a slash in the objectReference to extract the object type. e.g. record:host/ZG5zLmhvc3QkLl9kZWZhdWx0LmNvbS5pbmZvLmhvc3Qx
      const queries = {
        _function: 'restartservices',
        restart_option: 'RESTART_IF_NEEDED'
      };
      it('should error if - missing slash from objectReference', (done) => {
        try {
          a.restartServices('noslash', queries, (data, error) => {
            try {
              const errorMessage = 'extractObjectTypeFromObjectReference: could not extract type, expecting slash in objectReference';
              assert.equal(error, errorMessage);
              assert.equal(data, null);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
